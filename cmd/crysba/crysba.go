package main

import "github.com/hajimehoshi/ebiten/v2"
import "gitlab.com/crysba/crysba/art"
import "image"
import _ "image/png"
import _ "image/jpeg"
import "log"
import "bytes"
import _ "io"
import _ "fmt"

type Crysba int8

const CrysbaNone Crysba = -1
const CrysbaRed Crysba = 0
const CrysbaGreen Crysba = 1
const CrysbaBlue Crysba = 2
const CrysbaYellow Crysba = 3

const FieldWide = 13
const FieldHigh = 13
const ScreenWide = 640
const ScreenHigh = 480

type Level struct {
	Name   string
	Target Crysba
	Colors int
	Field  [FieldWide][FieldHigh]Crysba
}

var runeToCrysba map[rune]Crysba = map[rune]Crysba{
	'R': CrysbaRed,
	'r': CrysbaRed,
	'G': CrysbaGreen,
	'g': CrysbaRed,
	'B': CrysbaBlue,
	'b': CrysbaBlue,
	'Y': CrysbaYellow,
	'y': CrysbaYellow,
	'.': CrysbaNone,
}

/*
func RuneToCrysba(r rune) Crysba {
	switch r {
	case 'R', 'r':
		return CrysbaRed
	default:
		return CrysbaNone
	}
}

func ReadLevel(r io.Reader) (*Level, error) {
	level := &Level{}
	_, _ = fmt.Fscanf(r, "name %s", &level.Name)
	_, _ = fmt.Fscanf(r, "target %d", &level.Target)
	_, _ = fmt.Fscanf(r, "color %d", &level.Colors)
	for high := 0; high < FieldHigh; high++ {
		c := 'R'
		for wide := 0; wide < FieldWide; wide++ {
			_, _ = fmt.Fscanf(r, "%c ", &c)
			crysba, ok := RuneToCrysba[c]
			if !ok {
				crysba = CrysbaNone
			}
			level.Field[wide][high] = crysba
		}
		_, _ = fmt.Fscanf(r, "\n")
	}
	return level, nil
}
*/

// Game implements ebiten.Game interface.
type Game struct {
	Graphics struct {
		Background *ebiten.Image
		Orbs       [4]*ebiten.Image
	}
	Level
}

// Update proceeds the game state.
// Update is called every tick (1/60 [s] by default).
func (g *Game) Update() error {
	// Write your game's logical update.
	return nil
}

// Draw draws the game screen.
// Draw is called every frame (typically 1/60[s] for 60Hz display).
func (g *Game) Draw(screen *ebiten.Image) {
	opts := &ebiten.DrawImageOptions{}
	screen.DrawImage(g.Graphics.Background, opts)
}

// Layout takes the outside size (e.g., the window size) and returns the (logical) screen size.
// If you don't have to adjust the screen size with the outside size, just return a fixed size.
func (g *Game) Layout(outsideWidth, outsideHeight int) (screenWidth, screenHeight int) {
	return ScreenWide, ScreenHigh
}

func LoadImageAsset(name string) (*ebiten.Image, error) {
	data, err := art.Asset(name)
	if err != nil {
		return nil, err
	}
	reader := bytes.NewReader(data)
	ima, _, err := image.Decode(reader)
	if err != nil {
		return nil, err
	}
	return ebiten.NewImageFromImage(ima), nil
}

func (g *Game) Load() error {
	names := []string{
		"crysba_red.png",
		"crysba_green.png",
		"crysba_blue.png",
		"crysba_yellow.png",
	}
	crysbas := []Crysba{
		CrysbaRed,
		CrysbaGreen,
		CrysbaBlue,
		CrysbaYellow,
	}

	for i, name := range names {
		crysba := crysbas[i]
		img, err := LoadImageAsset(name)
		if err != nil {
			return err
		}
		g.Graphics.Orbs[crysba] = img
	}
	img, err := LoadImageAsset("background_forest.png")
	if err != nil {
		return err
	}
	g.Graphics.Background = img
	return nil
}

func main() {
	var err error
	game := &Game{}
	if err := game.Load(); err != nil {
		log.Fatal(err)
	}

	ebiten.SetWindowSize(ScreenWide, ScreenHigh)
	ebiten.SetWindowTitle("Crysba")
	// Call ebiten.RunGame to start your game loop.
	if err = ebiten.RunGame(game); err != nil {
		log.Fatal(err)
	}
}
