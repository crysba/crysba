module gitlab.com/crysba/crysba

go 1.13

require (
	github.com/go-bindata/go-bindata v3.1.2+incompatible // indirect
	github.com/hajimehoshi/ebiten/v2 v2.0.0
	github.com/peterh/liner v1.2.1
)
